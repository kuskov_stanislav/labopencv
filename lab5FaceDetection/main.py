import cv2 as cv
def main():

    face_cascade = cv.CascadeClassifier('haarcascade_frontalface_default.xml')
    smile_cascade = cv.CascadeClassifier('haarcascade_smile.xml')

    stream = cv.VideoCapture(0)  # camera number

    width = 640  # camera capture width/height
    height = 480

    widthFr = 40  # signs size
    heightFr = 40

    stream.set(3, width)
    stream.set(4, height)
    if stream.isOpened() == False:
        print "Cannot open input video"
        exit()
    frameNumber = 0
    photoKey=""

    while 1:
        key = cv.waitKey(1)
        if key==32:
            photoKey=32

        frameNumber += 1
        flag, imgFull = stream.read()

        imgFull2=imgFull.copy()

        if flag == False: break  # end of video

        if key == 27:  # Esc key to stop
            break
        else:
            print key
            gray=cv.cvtColor(imgFull, cv.COLOR_BGR2GRAY)
            faces = face_cascade.detectMultiScale(gray,scaleFactor=1.3, minNeighbors=4,minSize=(30, 30))

            for (x, y, w, h) in faces:
                cv.rectangle(imgFull, (x, y), (x + w, y + h), (255, 0, 0), 2)
                roi_gray = gray[y:y + h, x:x + w]
                roi_color = imgFull[y:y + h, x:x + w]
                smile = smile_cascade.detectMultiScale(roi_gray,scaleFactor=1.9, minNeighbors=10,minSize=(20, 20))
                for (ex, ey, ew, eh) in smile:
                    cv.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)
                if len(smile) != 0:
                    if photoKey and photoKey==32:
                        print "makephoto"
                        cv.imwrite(str(frameNumber)+"x.jpg",imgFull)
                        cv.imwrite(str(frameNumber)+"x2.jpg", imgFull2)

                        photoKey=""

            cv.imshow('imgFull', imgFull)




    cv.destroyAllWindows()


if __name__ == "__main__":
    main()