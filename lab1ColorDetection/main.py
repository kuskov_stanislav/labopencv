import cv2 as cv
import numpy as np
import signDetection as SD

ix,iy = -1,-1

def draw_circle(event,x,y,flags,param):
    if event == cv.EVENT_LBUTTONDOWN:
        global ix, iy
        ix, iy = x, y

def pixel_color():
    stream = cv.VideoCapture(0)  # camera number

    width = 640  # camera capture width/height
    height = 480

    stream.set(3, width)
    stream.set(4, height)
    if stream.isOpened() == False:
        print "Cannot open input video"
        exit()
    frameNumber = 0

    while 1:

        key = cv.waitKey(1)
        # frameNumber += 1
        flag, imgFull = stream.read()

        if key == 27:  # Esc key to stop
            break
        else:

            cv.setMouseCallback('imgFull', draw_circle, param=None)

            if ix > -1:
                colorArray = imgFull[iy, ix]
                cv.putText(imgFull, str(colorArray), (10, 10), cv.FONT_HERSHEY_COMPLEX_SMALL, fontScale=0.7,
                           color=(int(colorArray[0]), int(colorArray[1]), int(colorArray[2])), thickness=1)

            cv.imshow('imgFull', imgFull)

    cv.destroyAllWindows()

def color_obj_detection():

    detection = SD.signDetectorOpenCV()

    stream = cv.VideoCapture(0)  # camera number

    width = 640  # camera capture width/height
    height = 480


    widthFr = 40    #signs size
    heightFr = 40


    stream.set(3, width)
    stream.set(4, height)

    if stream.isOpened() == False:
        print "Cannot open input video"
        exit()

    frameNumber = 0

    while 1:
        key = cv.waitKey(1)
        # frameNumber += 1
        flag, imgFull = stream.read()

        if key == 27:  # Esc key to stop
            break
        else:

            frameNumber += 1
            flag, imgFull = stream.read()

            if flag == False: break  # end of video

            detection.videoCapture = imgFull  # detecting signs

            imgGeneralProcessing = detection.videoCaptureGeneralProcesing()

            signRoi = detection.imgSingDetecting(imgFull, imgGeneralProcessing, widthFr, heightFr)


            if signRoi!="":
                print "sdfsd"

            cv.imshow('imgFull', imgFull)

    cv.destroyAllWindows()



def main():
    color_obj_detection()

     # pixel_color()




if __name__ == "__main__":
    main()

