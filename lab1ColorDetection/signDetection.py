import numpy as np
import cv2 as cv
class signDetectorOpenCV:

    videoCapture=np.array([])

    def videoCaptureGeneralProcesing(self):

        # cv.imshow('input', self.videoCapture)
        hsv = cv.cvtColor(self.videoCapture, cv.COLOR_BGR2HSV)

        hsv = cv.blur(hsv, (5, 5))

        # lower_blue = np.array([20, 150, 80])  # camera web best
        # upper_blue = np.array([200, 255, 180])
        #
        lower_blue = np.array([90, 104, 78])  # camera web best
        upper_blue = np.array([255, 255, 255])

        # lower_blue = np.array([44, 137, 124])  # camera web best
        # upper_blue = np.array([132, 255, 255])


        threshBlue = cv.inRange(hsv, lower_blue, upper_blue)
        threshBlue = cv.erode(threshBlue, None, iterations=2)  # delete other white pixels
        threshBlue = cv.dilate(threshBlue, None, iterations=6)  # not change ROI blob size in prev function (erode)

        # cv.imshow('threshBlue', threshBlue)
        return threshBlue

    def imgSingDetecting(self,imgFull, imgGP, widthFr, heightFr):

        contours, hierarchy = cv.findContours(imgGP.copy(), cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

        for cnt in contours:
            moments = cv.moments(cnt)  # Calculate moments
            if moments['m00'] != 0:
                c = sorted(contours, key=cv.contourArea, reverse=True)[0]
                rect = cv.minAreaRect(c)
                ##### make rotation for rect or for box!!!!!!!
                box = np.int0(cv.cv.BoxPoints(rect))
                if not(box is None):

                    boxWidth = box[2][0] - box[1][0]
                    boxHeight = box[0][1] - box[1][1]

                    x1 = int(box[0][0])
                    y1 = int(box[0][1])
                    x2 = int(box[1][0])
                    y2 = int(box[1][1])
                    x3 = int(box[2][0])
                    y3 = int(box[2][1])
                    x4 = int(box[3][0])
                    y4 = int(box[3][1])

                    roi = imgFull[y2:y1, x2:x3]
                    roiImg = roi.copy()

                    cv.drawContours(imgFull, [box], -1, (0, 255, 0), 3)  # draw contours in green color

                    if not roiImg.any():
                        pass
                    else:
                        newimg = cv.resize(roiImg, (int(widthFr), int(heightFr)))

                        RECnewimg = newimg.copy()
                        return RECnewimg


    def imgSingRecAlg(self,singDetection,widthFr,heightFr):
        mark70 = cv.imread('signDetection/marks/70.jpg',2)
        mark70 = cv.resize(mark70,(int(100),int(100)))
        mark70 = cv.blur(mark70, (1, 1))
        mark70 = cv.threshold(mark70, 127, 255, cv.THRESH_BINARY)[1]

        markLeft = cv.imread('signDetection/marks/left.jpg',2)
        markLeft = cv.resize(markLeft,(int(100),int(100)))
        markLeft = cv.blur(markLeft, (1, 1))
        markLeft = cv.threshold(markLeft, 127, 255, cv.THRESH_BINARY)[1]

        RECnewimgGray = cv.cvtColor(singDetection, cv.COLOR_RGB2GRAY)
        RECnewimgBlur = cv.blur(RECnewimgGray, (1, 1))
        RECnewimgTresh = cv.threshold(RECnewimgBlur, 127, 255, cv.THRESH_BINARY)[1]
        RECnewimgHsv = np.float32(cv.cvtColor(singDetection, cv.COLOR_RGB2HSV))

        RECnewimgCanny = cv.Canny(cv.cvtColor(np.uint8(RECnewimgHsv*255), cv.COLOR_RGB2GRAY), 70, 170)


        counter = 0
        counter2 = 0

        for x in range((widthFr-1)/2):
            x=x+1
            for y in range((heightFr-1)):
                y=y+1
                if (RECnewimgTresh[x,y])!=(mark70[x,y]):

                # if RECnewimgTresh[y,x]!=0:
                     counter=counter+1
        if (counter<500):
            cv.imshow('sign',mark70)
            print "max speed 70 km/h"
        else:
            counter=0
            for x in range((widthFr-1)/2):
                x=x+1
                for y in range((heightFr-1)):
                    y=y+1
                    if (RECnewimgTresh[x,y])!=(markLeft[x,y]):
                        counter=counter+1

            # print "Left:"+str(counter)
            # if (counter2<9000):
            #     print "Left only"
            if (counter<500):
                cv.imshow('sign',markLeft)
                print "Left"
        print counter

        return RECnewimgTresh