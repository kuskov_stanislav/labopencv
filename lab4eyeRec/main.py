import cv2 as cv
import numpy as np

def FindSubImage(im1, im2):
    needle = im1
    haystack = im2

    result = cv.matchTemplate(needle,haystack,cv.TM_CCOEFF_NORMED)
    y,x = np.unravel_index(result.argmax(), result.shape)
    return x,y


def eye_detection():


    stream = cv.VideoCapture(0)  # camera number

    width = 640  # camera capture width/height
    height = 480


    widthFr = 40    #signs size
    heightFr = 40

    stream.set(3, width)
    stream.set(4, height)

    if stream.isOpened() == False:
        print "Cannot open input video"
        exit()

    frameNumber = 0

    while 1:
        key = cv.waitKey(1)
        # frameNumber += 1
        flag, imgFull = stream.read()

        if key == 27:  # Esc key to stop
            break
        else:

            frameNumber += 1
            flag, imgFull = stream.read()

            if flag == False: break  # end of video

            # eye matchTemplate

            tmp = cv.imread('eye_tmp.jpg', cv.CV_LOAD_IMAGE_UNCHANGED)

            x_eye,y_eye=FindSubImage(imgFull,tmp)
            # print x,y
            cv.circle(imgFull,(x_eye,y_eye),3,(0,255,0),-1)

            cv.imshow('imgFull', imgFull)
            cv.imshow('tmp', tmp)


    cv.destroyAllWindows()



def main():
    eye_detection()

     # pixel_color()




if __name__ == "__main__":
    main()

