import cv2 as cv
import numpy as np

ix,iy = -1,-1

def spoones_segmentation(img):

    hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)

    hsv = cv.blur(hsv, (10, 10))

    # lower_blue = np.array([20, 150, 80])  # camera web best
    # upper_blue = np.array([200, 255, 180])
    #
    lower_blue = np.array([30, 0, 145])  # camera web best
    upper_blue = np.array([135, 40, 255])

    # lower_blue = np.array([44, 137, 124])  # camera web best
    # upper_blue = np.array([132, 255, 255])


    threshBlue = cv.inRange(hsv, lower_blue, upper_blue)
    threshBlue = cv.erode(threshBlue, None, iterations=1)  # delete other white pixels
    threshBlue = cv.dilate(threshBlue, None, iterations=5)  # not change ROI blob size in prev function (erode)

    # cv.imshow('threshBlue', threshBlue)
    return threshBlue



def spoones():
    imgFull=cv.imread('test1.jpg')
    thresh = spoones_segmentation(imgFull)
    while 1:
        key = cv.waitKey(1)

        if key == 27:  # Esc key to stop
            break
        else:
            cv.imshow('trash', thresh)
    cv.destroyAllWindows()


    # stream = cv.VideoCapture(0)  # camera number
    #
    # width = 640  # camera capture width/height
    # height = 480
    #
    # stream.set(3, width)
    # stream.set(4, height)
    #
    # if stream.isOpened() == False:
    #     print "Cannot open input video"
    #     exit()
    #
    # frameNumber = 0
    #
    # while 1:
    #     key = cv.waitKey(1)
    #     # frameNumber += 1
    #     flag, imgFull = stream.read()
    #
    #     if key == 27:  # Esc key to stop
    #         break
    #     else:
    #
    #         frameNumber += 1
    #         flag, imgFull = stream.read()
    #
    #         if flag == False: break  # end of video
    #
    #         thresh=spoones_segmentation(imgFull)
    #         cv.imshow('thresh', thresh)
    #         cv.imshow('imgFull', imgFull)
    #
    # cv.destroyAllWindows()



def main():
    spoones()

     # pixel_color()




if __name__ == "__main__":
    main()

